import { UsersService } from './users.service';
import { ReservationsService } from './reservations.service';
import { Injectable } from '@angular/core';
import { Commentaire } from '../models/commentaire.model.ts';

@Injectable({
  providedIn: 'root'
})
export class CommentairesService {

  commentaires: Commentaire [] = [
    {
    id: 1,
    Contenu: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa, omnis. Est eaque, nulla consectetur corrupti dolor et dolorum, iusto aut exercitationem minima totam magnam. Iusto, est! Nemo optio cum a!',
    Evaluation: 2,
    reservation: this.reservationService.reservations[0],
    DateCommentaire: '14/09/2022'
    
  },
  {
    id: 2,
    Contenu: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa, omnis. Est eaque, nulla consectetur corrupti dolor et dolorum, iusto aut exercitationem minima totam magnam. Iusto, est! Nemo optio cum a!',
    Evaluation: 3,
    reservation: this.reservationService.reservations[3],
    DateCommentaire: '15/06/2022'
  
  },
  {
    id: 3,
    Contenu: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Culpa, omnis. Est eaque, nulla consectetur corrupti dolor et dolorum, iusto aut exercitationem minima totam magnam. Iusto, est! Nemo optio cum a!',
    Evaluation: 4,
    reservation: this.reservationService.reservations[4],
    DateCommentaire: '08/05/2022'
  }]
  constructor(private reservationService: ReservationsService, private userService: UsersService) { }
}
