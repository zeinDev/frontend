import { Injectable } from '@angular/core';
import { ServiceHabitat } from '../models/service-habitat.model.ts';

@Injectable({
  providedIn: 'root'
})
export class HabitatServicesService {
  servicesHabitat: ServiceHabitat[] = [
    {id: 1, nom: 'Petit-déjeuner', description: 'Pétit déjeuner servi dans', icon: 'petit-déjeuner'},
    {id: 1, nom: 'wifi', description: 'wifi gratuit 24h/24h', icon: 'wifi'},
    {id: 1, nom: 'parking', description: 'Parking 30m du cabane', icon: 'parking'},
    {id: 1, nom: 'Petit-déjeuner', description: 'Pétit déjeuner servi dans', icon: 'petit-déjeuner'},
    {id: 1, nom: 'wifi', description: 'wifi gratuit 24h/24h', icon: 'wifi'},
    {id: 1, nom: 'parking', description: 'Parking 30m du cabane', icon: 'parking'}
  ];
  services: ServiceHabitat[] =[
    {id: 1, nom: 'Petit-déjeuner', description: 'Pétit déjeuner servi dans', icon: 'petit-déjeuner'},
    {id: 1, nom: 'wifi', description: 'wifi gratuit 24h/24h', icon: 'wifi'}
  ]
  constructor() { }
}
