import { HabitatsService } from './habitats.service';
import { UsersService } from './users.service';
import { Reservation } from './../models/reservation.model.ts';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {

  reservations: Reservation[] =[
    {
      id: 3,
      DateArrivee: '12/09/2022',
      DateDepart: '16/09/2022',
      NombrePersonnes: 3,
      MontantTotal: 1200.40,
      DateReservation: '08/08/2022',
      Annulee: false,
      user: this.userService.users[1],
      habitat: this.habitatsService.habitats[1],
    },
    {
      id: 2,
      DateArrivee: '13/06/2022',
      DateDepart: '15/06/2022',
      NombrePersonnes: 4,
      MontantTotal: 1500.90,
      DateReservation: '10/06/2022',
      Annulee: true,
      user: this.userService.users[1],
      habitat: this.habitatsService.habitats[2],
    
    },
    {
      id: 0,
      DateArrivee: '02/05/2022',
      DateDepart: '09/05/2022',
      NombrePersonnes: 2,
      MontantTotal: 2000.30,
      DateReservation: '14/02/2022',
      Annulee: false,
      user: this.userService.users[1],
      habitat: this.habitatsService.habitats[3],
  
    },
    {
      id: 1,
      DateArrivee: '12/09/2022',
      DateDepart: '16/09/2022',
      NombrePersonnes: 3,
      MontantTotal: 4000.40,
      DateReservation: '08/08/2022',
      Annulee: true,
      user: this.userService.users[0],
      habitat: this.habitatsService.habitats[1],
    },
    {
      id: 5,
      DateArrivee: '13/06/2022',
      DateDepart: '15/06/2022',
      NombrePersonnes: 4,
      MontantTotal: 700.90,
      DateReservation: '10/06/2022',
      Annulee: true,
      user: this.userService.users[2],
      habitat: this.habitatsService.habitats[4],
    
    },
    {
      id: 4,
      DateArrivee: '02/05/2022',
      DateDepart: '09/05/2022',
      NombrePersonnes: 2,
      MontantTotal: 3000.30,
      DateReservation: '14/02/2022',
      Annulee: false,
      user: this.userService.users[1],
      habitat: this.habitatsService.habitats[6],
  
    }
   ] 
  constructor(private userService: UsersService, private habitatsService: HabitatsService) { }
}
