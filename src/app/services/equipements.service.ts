import { Equipement } from './../models/equipement.model.ts';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EquipementsService {

  equipements: Equipement[] = [
    {id: 1, nom: 'Litterie', icon: 'literie.png'},
    {id: 2, nom: 'Television', icon: 'television.png'},
    {id: 3, nom: 'Barbecue', icon: 'barbecue.png'},
    {id: 1, nom: 'Litterie', icon: 'literie.png'},
    {id: 2, nom: 'Television', icon: 'television.png'},
    {id: 3, nom: 'Barbecue', icon: 'barbecue.png'},
    {id: 1, nom: 'Litterie', icon: 'literie.png'},
    {id: 2, nom: 'Television', icon: 'television.png'},
    {id: 3, nom: 'Barbecue', icon: 'barbecue.png'},
    {id: 1, nom: 'Litterie', icon: 'literie.png'},
    {id: 2, nom: 'Television', icon: 'television.png'},
    {id: 3, nom: 'Barbecue', icon: 'barbecue.png'}
  ];
  equipementstwo : Equipement[] = [  
    {id: 1, nom: 'Litterie', icon: 'literie.png'},
    {id: 2, nom: 'Television', icon: 'television.png'},
    {id: 3, nom: 'Barbecue', icon: 'barbecue.png'},  
  ]
  constructor() { }
  
}
