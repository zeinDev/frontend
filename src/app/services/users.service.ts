import { MediasService } from './medias.service';
import { User } from './../models/user.model.ts';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  users: User[]= [{
    id: 1,
    email: 'zeinlemine@gmail.com',
    roles: ['Admin'],
    password: 'zein0909',
    username: 'Zeinles',
    media: this.media.mediasU[1]
  },
  {
    id: 2,
    email: 'Maxim347@yahoo.fr',
    roles: ['Locataire'],
    password: 'maxim124768',
    username: 'maxiiim',
    media: this.media.mediasU[0],
  
  },
  {
    id: 3,
    email: 'PauloHerix@yahoo.fr',
    roles: ['Propriétaire'],
    password: 'paulololo',
    username: 'Peuls',
    media: this.media.mediasU[2],
  
  },
  {
    id: 4,
    email: 'MohamedAli@yahoo.fr',
    roles: ['Admin','Propriétaire'],
    password: 'Alimed',
    username: 'Mohamed_ali',
    media: this.media.mediasU[1],
  }
]
  constructor(private media: MediasService) { }
}
