import { Categorie } from './../models/categorie.model.ts';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  categories: Categorie[] = [
    {  id: 1, nom: "Cabane sur l'eau", image: 'cabane_leau.jpg'},
    {  id: 2, nom: 'Cabane deans les arbres', image: 'YOURT.jpg'},
    {  id: 3, nom: 'Yourte', image: 'YOURT.jpg'},
    {  id: 4, nom: 'Kota', image: 'KOTA.jpg'},
    {  id: 5, nom: 'Tipi', image: 'TIPI.jpg'},
    {  id: 6, nom: 'Tiny house', image: 'TINY.jpg'},
    {  id: 7, nom: 'Tonneau', image: 'TONNEAU.jpg'},
    {  id: 8, nom: 'Tente', image: 'TENTE.jpg'},
    {  id: 9, nom: 'Chalet', image: 'CHALET.jpg'},
    {  id: 10, nom: 'Dome', image: 'Dome.jpg'},
    {  id: 11, nom: 'Bulle', image: 'Bulle.jpg'},
    {  id: 12, nom: 'Inclassable', image: 'inclassble.jpeg'}
  ]
  constructor() { }
}
