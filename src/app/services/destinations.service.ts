import { Destination } from './../models/destination.model.ts';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DestinationsService {

  destinations: Destination[] = [
    {id: 1, ville: 'clermont', departement: 63, pays: 'france'},
    { id: 2, ville: 'Marseille', departement: 13, pays: 'france' },
    { id: 3, ville: 'Toulon', departement: 83, pays: 'france' },
    { id: 4, ville: 'Nice', departement: 6, pays: 'france' },

  ] 
  constructor() { }
}
