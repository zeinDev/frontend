import { Media } from './../models/media.model.ts';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MediasService {
  mediasH: Media[] =[
    {id: 1, fichier:'01.jpg'},
    {id: 2, fichier:'02.jpg'},
    {id: 3, fichier:'03.jpg'},
    {id: 4, fichier:'04.jpg'},
    {id: 5, fichier:'1.jpg'},
    {id: 6, fichier:'1.jpg'},
    {id: 7, fichier:'2.jpg'},
    {id: 8, fichier:'4.jpg'},
    {id: 9, fichier:'1.jpg'},
    {id: 10, fichier:'2.jpg'},
    {id: 11, fichier:'3.jpg'},
    {id: 12, fichier:'1.jpg'},
    {id: 13, fichier:'2.jpg'},
    {id: 14, fichier:'1.jpg'},
    {id: 15, fichier:'2.jpg'},
    {id: 16, fichier:'4.jpg'},
    {id: 17, fichier:'5.jpg'},
    {id: 18, fichier:'1.jpg'},
    {id: 19, fichier:'2.jpg'},
    {id: 20, fichier:'1.jpg'},
    {id: 21, fichier:'2.jpg'},
    {id: 22, fichier:'3.jpg'},
  ];
  mediasU: Media[] = [
    {id: 6, fichier:'01.jpg'},
    {id: 10, fichier:'02.jpg'},
    {id: 9, fichier:'03.jpg'},
    {id: 16, fichier:'04.jpg'}
  ]
  constructor() { }
}
