import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SignUpLocataireComponent } from './Auth/sign-up-locataire/sign-up-locataire.component';
import { SignUpProprietaireComponent } from './Auth/sign-up-proprietaire/sign-up-proprietaire.component';
import { SignInComponent } from './Auth/sign-in/sign-in.component';
import { AccueilComponent } from './accueil/accueil.component';
import { HebergementsComponent } from './hebergements/hebergements.component';
import { HabitatListComponent } from './hebergements/habitat-list/habitat-list.component';
import { HabitatItemComponent } from './hebergements/habitat-item/habitat-item.component';
import { ContactComponent } from './hebergements/habitat-item/contact/contact.component';
import { AddReservationComponent } from './hebergements/habitat-item/add-reservation/add-reservation.component';
import { EquipementsComponent } from './hebergements/habitat-item/equipements/equipements.component';
import { HabitatServicesComponent } from './hebergements/habitat-item/habitat-services/habitat-services.component';
import { CommentairesComponent } from './hebergements/habitat-item/commentaires/commentaires.component';
import { DestinationsComponent } from './destinations/destinations.component';
import { ContactezNousComponent } from './contactez-nous/contactez-nous.component';
import { QuiSommesNousComponent } from './qui-sommes-nous/qui-sommes-nous.component';
import { MentionsLegalesComponent } from './mentions-legales/mentions-legales.component';
import { CGUComponent } from './cgu/cgu.component';
import { CGVComponent } from './cgv/cgv.component';
import { LocMenuComponent } from './Locataire/loc-menu/loc-menu.component';
import { MesReservationsComponent } from './Locataire/mes-reservations/mes-reservations.component';
import { MaReservationComponent } from './Locataire/ma-reservation/ma-reservation.component';
import { AddCommentaireComponent } from './Locataire/add-commentaire/add-commentaire.component';
import { PropMenuComponent } from './Proprietaire/prop-menu/prop-menu.component';
import { ReservationsListComponent } from './Proprietaire/reservations-list/reservations-list.component';
import { ReservationItemComponent } from './Proprietaire/reservation-item/reservation-item.component';
import { MesHabitatsComponent } from './Proprietaire/mes-habitats/mes-habitats.component';
import { MonHabiatComponent } from './Proprietaire/mon-habiat/mon-habiat.component';
import { AddHabitatComponent } from './Proprietaire/add-habitat/add-habitat.component';
import { EditHabitatComponent } from './Proprietaire/edit-habitat/edit-habitat.component';
import { NotificationListComponent } from './Proprietaire/notification-list/notification-list.component';
import { NotificationItemComponent } from './Proprietaire/notification-item/notification-item.component';
import { DashboardComponent } from './Admin/dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { CategoriesService } from './services/categories.service';
import { CommentairesService } from './services/commentaires.service';
import { DestinationsService } from './services/destinations.service';
import { EquipementsService } from './services/equipements.service';
import { HabitatServicesService } from './services/habitat-services.service';
import { HabitatsService } from './services/habitats.service';
import { MediasService } from './services/medias.service';
import { ReservationsService } from './services/reservations.service';
import { HabitatsComponent } from './Admin/dashboard/habitats/habitats.component';
import { UsersComponent } from './Admin/dashboard/users/users.component';
import { CategoryComponent } from './Admin/dashboard/category/category.component';
import { ReservationsComponent } from './Admin/dashboard/reservations/reservations.component';
import { NotifierComponent } from './Admin/dashboard/notifier/notifier.component';
import { BoxMailComponent } from './Admin/dashboard/box-mail/box-mail.component';
import { CommentsComponent } from './Admin/dashboard/comments/comments.component';
import { ListDestinationsComponent } from './Admin/dashboard/list-destinations/list-destinations.component';
import { ListEquipementsComponent } from './Admin/dashboard/list-equipements/list-equipements.component';
import { ListServicesComponent } from './Admin/dashboard/list-services/list-services.component';
import { ClientComponent } from './client/client.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SignUpLocataireComponent,
    SignUpProprietaireComponent,
    SignInComponent,
    AccueilComponent,
    HebergementsComponent,
    HabitatListComponent,
    HabitatItemComponent,
    ContactComponent,
    AddReservationComponent,
    EquipementsComponent,
    HabitatServicesComponent,
    CommentairesComponent,
    DestinationsComponent,
    ContactezNousComponent,
    QuiSommesNousComponent,
    MentionsLegalesComponent,
    CGUComponent,
    CGVComponent,
    LocMenuComponent,
    MesReservationsComponent,
    MaReservationComponent,
    AddCommentaireComponent,
    PropMenuComponent,
    ReservationsListComponent,
    ReservationItemComponent,
    MesHabitatsComponent,
    MonHabiatComponent,
    AddHabitatComponent,
    EditHabitatComponent,
    NotificationListComponent,
    NotificationItemComponent,
    DashboardComponent,
    HabitatsComponent,
    UsersComponent,
    CategoryComponent,
    ReservationsComponent,
    NotifierComponent,
    BoxMailComponent,
    CommentsComponent,
    ListDestinationsComponent,
    ListEquipementsComponent,
    ListServicesComponent,
    ClientComponent,
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: 'Client', 
      component: ClientComponent,
      children: [
        {path: 'auth/signupLocataire', component: SignUpLocataireComponent},
        {path: 'auth/signupProprietaire', component: SignUpProprietaireComponent },
        {path: 'auth/signin', component: SignInComponent},
        {path: 'habitat', component: HabitatItemComponent},
        { path: 'hebergements', component: HebergementsComponent},
        {path: 'list', component: HabitatListComponent}
      ]
    },
    {
        path: 'Locataire/maReservation', 
        component: MaReservationComponent,
        children: [
         {path: 'commenter', component: AddCommentaireComponent}
       ]
      },
       {
        path: 'Admin', 
        component: DashboardComponent,
        children: [
          {path: 'habitats', component: HabitatsComponent},
         {path: 'users', component: UsersComponent},
         {path: 'categories', component: CategoryComponent},
         {path: 'destinations', component: ListDestinationsComponent},
         {path: 'reservations', component: ReservationsComponent},
         {path: 'commentaires', component: CommentsComponent},
         {path: 'equipements', component: ListEquipementsComponent},
         {path: 'services', component: ListServicesComponent},
         {path: 'notifier', component: NotifierComponent},
         {path: 'mail', component: BoxMailComponent}

       ]
       }
    ])
    
  ],
  providers: [
    AuthGuardService,
    AuthService,
    CategoriesService,
    CommentairesService,
    DestinationsService,
    EquipementsService,
    HabitatServicesService,
    HabitatsService,
    MediasService,
    ReservationsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
