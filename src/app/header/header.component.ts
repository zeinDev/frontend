import { Component, OnInit } from '@angular/core';
import { faMicrosoft } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  toggler = false;
  faMicrosoft = faMicrosoft;
  constructor() { }

  ngOnInit(): void {
  }

  menuToggle() {
    this.toggler = !this.toggler;
  }
 

}
