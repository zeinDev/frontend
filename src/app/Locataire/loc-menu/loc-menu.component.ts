import { Component, OnInit } from '@angular/core';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { faHouseUser } from '@fortawesome/free-solid-svg-icons';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-loc-menu',
  templateUrl: './loc-menu.component.html',
  styleUrls: ['./loc-menu.component.css']
})
export class LocMenuComponent implements OnInit {


  faUserCircle = faUserCircle
  faHouseUser = faHouseUser
  faSignOutAlt = faSignOutAlt
  toggler = false ;

  constructor() { }
 
  ngOnInit(): void {
  }
  menuToggle() {
    this.toggler = !this.toggler;
  }

}
