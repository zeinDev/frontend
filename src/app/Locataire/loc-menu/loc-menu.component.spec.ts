import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocMenuComponent } from './loc-menu.component';

describe('LocMenuComponent', () => {
  let component: LocMenuComponent;
  let fixture: ComponentFixture<LocMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
