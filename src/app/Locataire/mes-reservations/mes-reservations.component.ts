import { UsersService } from './../../services/users.service';
import { User } from './../../models/user.model.ts';
import { ReservationsService } from './../../services/reservations.service';
import { Reservation } from './../../models/reservation.model.ts';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mes-reservations',
  templateUrl: './mes-reservations.component.html',
  styleUrls: ['./mes-reservations.component.css']
})
export class MesReservationsComponent implements OnInit {

  reservations: Reservation[]=[]
  user!: User 
  constructor(private reservationService: ReservationsService, private userService: UsersService) { }

  ngOnInit(): void {
    this.user = this.userService.users[1];
    this.recupereReservations()
  }
  /** recuperer les reservations d'un client */
  recupereReservations(){
    for(let reservation of this.reservationService.reservations){
      if(reservation.user === this.user) {
        this.reservations.push(reservation)
      }
    }
  }

}
