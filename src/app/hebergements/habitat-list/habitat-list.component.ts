import { HabitatsService } from './../../services/habitats.service';
import { Habitat } from 'src/app/models/habitat.model.ts';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-habitat-list',
  templateUrl: './habitat-list.component.html',
  styleUrls: ['./habitat-list.component.css']
})
export class HabitatListComponent implements OnInit {

  habitats!: Habitat[];
  constructor(private habitatService: HabitatsService) { }

  ngOnInit(): void {
    this.habitats = this.habitatService.habitats;
  }

}
