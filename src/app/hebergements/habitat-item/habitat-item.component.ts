import { CommentairesService } from './../../services/commentaires.service';
import { Commentaire } from './../../models/commentaire.model.ts';
import { HabitatsService } from './../../services/habitats.service';
import { Component, OnInit } from '@angular/core';
import { Habitat } from 'src/app/models/habitat.model.ts';
import { faUsers } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-habitat-item',
  templateUrl: './habitat-item.component.html',
  styleUrls: ['./habitat-item.component.css']
})
export class HabitatItemComponent implements OnInit {

  habitat!: Habitat;
  faUsers = faUsers;
  commentaires: Commentaire[]=[]
  image!: string  ;
  images: string[]=[];
  counter: number = 0;

  constructor(private habitatService: HabitatsService, private commService: CommentairesService) { }
  
  ngOnInit(): void {
    this.habitat = this.habitatService.habitat;
    this.image = this.habitat.medias[0].fichier 
    this.recupereCommentaires()
    this.recuperImages();
  }  

  /** recuperer les commentaires de l'habitat */
  recupereCommentaires(){
        for(let commentaire of this.commService.commentaires ){
          if(commentaire.reservation.habitat === this.habitat){
            this.commentaires.push(commentaire);
          }
        } 
  }
  /** recuperer les images de l'habitat */
  recuperImages(){
    for(let i=0; i < this.habitat.medias.length; i++){
      this.images.push(this.habitat.medias[i].fichier)
  }
  }
  defilerGauche(){
    this.counter++;
    if(this.counter === this.images.length ) {
        this.counter = 0
    } 
    this.image = this.images[this.counter]; 
    
  }
  defilerDroit() {
    this.counter--;
    if(this.counter === -1 ) {
      this.counter = this.images.length - 1;
    } 
  this.image = this.images[this.counter]; 
}
}
