import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up-proprietaire',
  templateUrl: './sign-up-proprietaire.component.html',
  styleUrls: ['./sign-up-proprietaire.component.css']
})
export class SignUpProprietaireComponent implements OnInit {

  signupForm!: FormGroup ;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(){
    this.signupForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', Validators.required],
      tel: ['', Validators.required],
      password: ['', Validators.required],
      confirmed_password: ['', Validators.required]      
    })
  }
  savePropData(){

  }
}
