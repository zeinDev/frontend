import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up-locataire',
  templateUrl: './sign-up-locataire.component.html',
  styleUrls: ['./sign-up-locataire.component.css']
})
export class SignUpLocataireComponent implements OnInit {

  signupForm!: FormGroup ;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.signupForm = this.formBuilder.group({
      nom: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmed_password: ['', Validators.required]      
    })
  }
  saveLocData(){

  }

}
