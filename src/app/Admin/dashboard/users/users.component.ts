import { UsersService } from './../../../services/users.service';
import { User } from './../../../models/user.model.ts';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users!: User[]
  constructor(private userService: UsersService) { }

  ngOnInit(): void {
    this.users = this.userService.users
  }

}
