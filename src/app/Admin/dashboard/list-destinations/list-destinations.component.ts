import { DestinationsService } from './../../../services/destinations.service';
import { Destination } from './../../../models/destination.model.ts';
import { Component, OnInit } from '@angular/core';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-list-destinations',
  templateUrl: './list-destinations.component.html',
  styleUrls: ['./list-destinations.component.css']
})
export class ListDestinationsComponent implements OnInit {

  edit = faEdit
  delete = faTrash
  destinations!: Destination[]
  constructor(private destinationS: DestinationsService) { }

  ngOnInit(): void {
    this.destinations = this.destinationS.destinations
  }

}
