import { HabitatsService } from './../../../services/habitats.service';
import { HabitatServicesService } from './../../../services/habitat-services.service';
import { Component, OnInit } from '@angular/core';
import { Habitat } from 'src/app/models/habitat.model.ts';

@Component({
  selector: 'app-habitats',
  templateUrl: './habitats.component.html',
  styleUrls: ['./habitats.component.css']
})
export class HabitatsComponent implements OnInit {

  habitats!: Habitat[] 
  constructor(private habitatService: HabitatsService) { }

  ngOnInit(): void {
    this.habitats = this.habitatService.habitats
  }

}
