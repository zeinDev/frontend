import { ReservationsService } from './../../../services/reservations.service';
import { Reservation } from './../../../models/reservation.model.ts';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  reservations!: Reservation[]
  constructor(private reservationService: ReservationsService) { }

  ngOnInit(): void {
    this.reservations = this.reservationService.reservations
  }

}
