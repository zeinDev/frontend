import { Component, OnInit } from '@angular/core';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { ServiceHabitat } from 'src/app/models/service-habitat.model.ts';
import { HabitatServicesService } from 'src/app/services/habitat-services.service';
@Component({
  selector: 'app-list-services',
  templateUrl: './list-services.component.html',
  styleUrls: ['./list-services.component.css']
})
export class ListServicesComponent implements OnInit {

  
  edit = faEdit
  delete = faTrash
  services!: ServiceHabitat[]
  constructor(private serviceHS: HabitatServicesService) { }

  ngOnInit(): void {
    this.services = this.serviceHS.servicesHabitat
  }

}
