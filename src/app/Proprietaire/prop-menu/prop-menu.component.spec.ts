import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PropMenuComponent } from './prop-menu.component';

describe('PropMenuComponent', () => {
  let component: PropMenuComponent;
  let fixture: ComponentFixture<PropMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
