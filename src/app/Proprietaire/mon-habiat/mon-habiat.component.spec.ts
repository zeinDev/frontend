import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonHabiatComponent } from './mon-habiat.component';

describe('MonHabiatComponent', () => {
  let component: MonHabiatComponent;
  let fixture: ComponentFixture<MonHabiatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonHabiatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonHabiatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
