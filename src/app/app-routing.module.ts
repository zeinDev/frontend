import { MaReservationComponent } from './Locataire/ma-reservation/ma-reservation.component';
import { AddCommentaireComponent } from './Locataire/add-commentaire/add-commentaire.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SignUpLocataireComponent } from './Auth/sign-up-locataire/sign-up-locataire.component';
import { SignUpProprietaireComponent } from './Auth/sign-up-proprietaire/sign-up-proprietaire.component';
import { SignInComponent } from './Auth/sign-in/sign-in.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot([
      {path: 'auth/signupLocataire', component: SignUpLocataireComponent},
      {path: 'auth/signupProprietaire', component: SignUpProprietaireComponent },
      {path: 'auth/signin', component: SignInComponent},
      {
       path: 'Locataire/maReservation', 
       component: MaReservationComponent,
       children: [
        {path: 'commenter', component: AddCommentaireComponent}
      ]}
    ])

  ]
})
export class AppRoutingModule { }
