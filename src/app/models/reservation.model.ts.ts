import { User } from './user.model.ts';
import { Habitat } from "./habitat.model.ts";

export class Reservation {
    constructor(
        public id: number,
        public DateArrivee: string, 
        public DateDepart: string, 
        public NombrePersonnes: number,
        public MontantTotal: number,
        public DateReservation: string,
        public Annulee : boolean,
        public user : User,
        public habitat: Habitat,
        ) {

    }
}
