import { Media } from './media.model.ts';
import { Reservation } from './reservation.model.ts';
import { Habitat } from "./habitat.model.ts";

export class User {
    constructor(
        public id: number,
        public email: string, 
        public roles: string[]=[], 
        public password: string,
        public username: string,
        public media: Media,
        ) {

    }
}
